# Kalman Filter Practice
By Mike Zhu

### Dependency
#### Python
* matplotlib

#### C++
* CMake 3.5+
* Eigen 3

### Build & Run
`mkdir build && cd build`
`cmake .. && make`
`./runKF`

### OutPut
The output file is in `data/KFresult.csv`

| Predicted x | Predicted y | Filtered x | Filtered y |
|:-----------:|:-----------:|:----------:| ----------:|
|     0       |       0     |     x0     |     y0     |

