import sys
import csv
import matplotlib.pyplot as plt

xkRaw = []
ykRaw = []

xk = []
yk = []

xkp = []
ykp = []

"""
    Load raw data to xkRaw and ykRaw
    ---
    xkRaw: raw xk
    ykRaw: raw yk
"""
with open('../data/measurement.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        row = row[0].split(';')
        xkRaw += [float(row[0])]
        ykRaw += [float(row[1])]


"""
    Load KF result. for each row: predicted x, y, filtered x, y
    ---
    xkp: predicted x
    ykp: predicted y
    xk: filtered x
    yk: filtered y
"""
with open('../data/KFresult.csv', newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        row = row[0].split(';')
        xkp += [float(row[0])]
        ykp += [float(row[1])]
        xk += [float(row[2])]
        yk += [float(row[3])]

# Calculate correction
xCorrection = [a - b for a, b in zip(xk, xkRaw)]
yCorrection = [a - b for a, b in zip(yk, ykRaw)]

# add number of iteration for x axis
time = [x for x in range(0, len(xk))]

plt.figure(1)  # Plot raw data
plt.subplot(311)
plt.plot(time, xkRaw, 'ro', time, xkRaw, 'b')
plt.title('xraw')

plt.subplot(312)
plt.plot(time, ykRaw, 'ro', time, ykRaw, 'b')
plt.title('yraw')

plt.subplot(313)
plt.plot(xkRaw, ykRaw, 'bx')
plt.title('xraw,yraw 2D')

plt.figure(2)  # Plot KF prediction
plt.subplot(311)
plt.plot(time, xkp, 'ro', time, xkp, 'b')
plt.title('predicted x')

plt.subplot(312)
plt.plot(time, ykp, 'ro', time, ykp, 'b')
plt.title('predicted y')

plt.subplot(313)
plt.plot(xkRaw, ykRaw, 'bx', label="raw")
plt.plot(xkp, ykp, 'gx', label="predicted")
plt.title('x,y 2D')
plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)


plt.figure(3)  # Plot KF correction
plt.subplot(311)
plt.plot(time, xCorrection, 'ro', time, xCorrection, 'b')
plt.title('corrected delta x')

plt.subplot(312)
plt.plot(time, yCorrection, 'ro', time, yCorrection, 'b')
plt.title('corrected delta y')

plt.subplot(313)
plt.plot(xkRaw, ykRaw, 'bx', label="raw")
plt.plot(xk, yk, 'gx', label="filtered")
plt.title('x,y 2D')
plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)

plt.show()
