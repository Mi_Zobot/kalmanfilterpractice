//
// Created by mzhu on 10/14/18.
//

#include "kalmanFilter.h"

#include <iostream>


void KalmanFilter::Init(Eigen::VectorXd const &x_in, Eigen::MatrixXd const &P_in, Eigen::MatrixXd const &F_in,
                        Eigen::MatrixXd const &H_in, Eigen::MatrixXd const &R_in, Eigen::MatrixXd const &Q_in)
{
    x_ = x_in;
    P_ = P_in;
    F_ = F_in;
    H_ = H_in;
    R_ = R_in;
    Q_ = Q_in;
}


void KalmanFilter::Predict()
{
    /*
     * predict the state
     */
    x_ = F_ * x_;
    Eigen::MatrixXd Ft = F_.transpose();
    P_ = F_ * P_ * Ft + Q_;
}


void KalmanFilter::Update(const Eigen::VectorXd &z)
{
    /*
     * update the state by using Kalman Filter equations
     */
    Eigen::VectorXd z_pred = H_ * x_;
    Eigen::VectorXd y = z - z_pred;

    Eigen::MatrixXd Ht = H_.transpose();
    Eigen::MatrixXd PHt = P_ * Ht;

    Eigen::MatrixXd S = H_ * PHt + R_;
    Eigen::MatrixXd Si = S.inverse();

    Eigen::MatrixXd K = PHt * Si;
    //new estimate
    x_ = x_ + (K * y);
    long x_size = x_.size();
    Eigen::MatrixXd I = Eigen::MatrixXd::Identity(x_size, x_size);
    P_ = (I - K * H_) * P_;
}
