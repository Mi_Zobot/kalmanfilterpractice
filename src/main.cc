#include "kalmanFilter.h"

#include <Eigen/Dense>

#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>


void loadData(std::string const &filepath,
              std::vector<double> &xRaw, std::vector<double> &yRaw)
{
    std::ifstream infile(filepath);
    std::string line;
    while (getline(infile,line)){
        std::stringstream s(line);
        for (int i = 0; i < 2; i++){
            std::string item;
            getline(s,item,';');  // Input stream, Buffer to store into, Widens characters
            if (i == 0) {
                // for xk
                xRaw.push_back(stod(item));
            }
            else {
                // for yk
                yRaw.push_back(stod(item));
            }
        }
    }
}


int main () {
    // load data
    std::string const filepath = "../data/measurement.csv";
    std::vector<double> xRaw;
    std::vector<double> yRaw;
    loadData(filepath, xRaw, yRaw);

    // set given Parameter
    double const x0 = xRaw[0];
    double const y0 = yRaw[0];
    double const deltaT = 5;                // sec
    double const sigmaSq = 50 * 50;         // sigma : m^2
    double const vSq = 314.0 * 314.0;       // v : m/s
    double const qSq = 9.12 * 9.12;         // q : m/s^2
    double const theta = 60;                // sec

    /*****************************************************************************
     *  Kalmen Filter initial Parameter
     ****************************************************************************/
    // Initial state
    Eigen::VectorXd initX(6);               // [x, y, vx, vy, ax, ay]
    initX << x0, y0, 0, 0, 0, 0;

    // Initial state covariance
    Eigen::MatrixXd initP = Eigen::MatrixXd::Identity(6, 6);
    initP(0, 0) = initP(1, 1) = sigmaSq;
    initP(2, 2) = initP(3, 3) = vSq;
    initP(4, 4) = initP(5, 5) = qSq;

    // Transition matrix
    Eigen::MatrixXd initF = Eigen::MatrixXd::Identity(6, 6);
    initF(0, 2) = initF(1, 3) = deltaT;
    initF(2, 4) = initF(3, 5) = deltaT;
    initF.topRightCorner(2, 2) = 0.5 * deltaT * deltaT * Eigen::MatrixXd::Identity(2, 2);
    initF.bottomRightCorner(2, 2) *= std::exp(-deltaT / theta);

    // Measurement matrix (x, y)
    Eigen::MatrixXd initH = Eigen::MatrixXd::Identity(2, 6);

    // State covariance
    Eigen::MatrixXd initR(2,2);
    initR << sigmaSq, 0,
             0, sigmaSq;

    // Process covariance matrix
    Eigen::MatrixXd initQ = Eigen::MatrixXd::Zero(6, 6);
    initQ(4, 4) = initQ(5, 5) = qSq * (1 - std::exp(-2 * deltaT / theta));

    /*****************************************************************************
     *  Run Kalmen Filter and record
     ****************************************************************************/
    std::ofstream writer("../data/KFresult.csv");
    if (!writer) {
        std::cout << "Error Opening File." << std::endl;
        return -1;
    }

    KalmanFilter kf;
    bool is_initialized = false;
    Eigen::VectorXd measurement(2);

    std::string line;  // output file format: predicted x, y, filtered x,y
    for (size_t i = 0; i < xRaw.size(); ++i) {
        // clear string for new line
        line.clear();
        if (!is_initialized) {
            kf.Init(initX, initP, initF, initH, initR, initQ);
            is_initialized = true;
            line = "0;0;" + std::to_string(kf.x_(0)) + ";" + std::to_string(kf.x_(1));
            writer << line << std::endl;
            continue;
        }

        kf.Predict();
        line = std::to_string(kf.x_(0)) + ";" + std::to_string(kf.x_(1)) + ";";

        measurement << xRaw[i], yRaw[i];
        kf.Update(measurement);
        line += std::to_string(kf.x_(0)) + ";" + std::to_string(kf.x_(1));
        writer << line << std::endl;
        //cout << "i = " << i << " " << kf.x_(0) << " " << kf.x_(1) << endl;
    }

    writer.close();
    std::cout << "Finsihed. File writen to ../data/KFresult.csv" << std::endl;
    return 0;
};
