#ifndef KALMENFILTER_KALMANFILTER_H
#define KALMENFILTER_KALMANFILTER_H
#include <Eigen/Dense>

class KalmanFilter {
public:
    // state vector
    Eigen::VectorXd x_;

    KalmanFilter() = default;;

    ~ KalmanFilter() = default;;

    /**
     * Init Initializes Kalman filter
     * @param x_in Initial state
     * @param P_in Initial state covariance
     * @param F_in Transition matrix
     * @param H_in Measurement matrix
     * @param R_in Measurement covariance matrix
     * @param Q_in Process covariance matrix
     */
    void Init(Eigen::VectorXd const &x_in, Eigen::MatrixXd const &P_in, Eigen::MatrixXd const &F_in,
              Eigen::MatrixXd const &H_in, Eigen::MatrixXd const &R_in, Eigen::MatrixXd const &Q_in);

    /**
     * Prediction Predicts the state and the state covariance
     * using the process model
     * @param delta_T Time between k and k+1 in s
     */
    void Predict();

    /**
     * Updates the state by using standard Kalman Filter equations
     * @param z The measurement at k+1
     */
    void Update(const Eigen::VectorXd &z);

private:
    // state covariance matrix
    Eigen::MatrixXd P_;

    // state transition matrix
    Eigen::MatrixXd F_;

    // process covariance matrix
    Eigen::MatrixXd Q_;

    // measurement matrix
    Eigen::MatrixXd H_;

    // measurement covariance matrix
    Eigen::MatrixXd R_;
};


#endif //KALMENFILTER_KALMANFILTER_H
